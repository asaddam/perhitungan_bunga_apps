package com.apb.interestcalc;

import com.apb.interestcalc.controller.MainController;
import com.apb.interestcalc.model.User;
import com.apb.interestcalc.repository.InsertRoleRepository;
import com.apb.interestcalc.repository.RoleRepository;
import com.apb.interestcalc.repository.UserRepository;
import com.apb.interestcalc.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.BDDMockito.given;

@WebMvcTest(controllers = MainController.class)
@ActiveProfiles("test")
public class TestController {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private RoleRepository roleRepository;

    @MockBean
    private InsertRoleRepository insertRoleRepository;

    private List<User> listUser;

    @Test
    void listUser() throws Exception {
        given(userRepository.findAll()).willReturn(listUser);
    }
}
