package com.apb.interestcalc.controller;

import com.apb.interestcalc.config.IAuthenticationFacade;
import com.apb.interestcalc.model.BalanceVAHistory;
import com.apb.interestcalc.model.BungaVA;
import com.apb.interestcalc.model.MappingPollingAcc;
import com.apb.interestcalc.model.ReadFileHistory;
import com.apb.interestcalc.repository.BalanceVARepository;
import com.apb.interestcalc.repository.BungaVARepository;
import com.apb.interestcalc.repository.FileHistoryRepository;
import com.apb.interestcalc.service.BalanceVAHisService;
import com.apb.interestcalc.service.BungaVAService;
import com.apb.interestcalc.service.ReadFileHisService;
import com.apb.interestcalc.utils.ReadFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Stream;

@Controller
public class BalanceVAHisController {
    private static Logger logger = LogManager.getLogger(BalanceVAHisController.class.getName());

    @Autowired
    private BalanceVAHisService balanceService;

    @Autowired
    private BalanceVARepository balanceRepo;

    @Autowired
    private ReadFileHisService fileService;

    @Autowired
    private FileHistoryRepository fileRepo;

    @Autowired
    private BungaVAService bungaVAService;

    @Autowired
    private BungaVARepository bungaVARepo;

    @Autowired
    private IAuthenticationFacade auth;

    //load .txt dari folder ke DB
    @RequestMapping(value = "/web/load-data-va")
    public String insertDataFile(Model model) {
        BufferedReader br;
        String sCurrentLine;
        int maxDataLength=11;

        Authentication authentication = auth.getAuthentication();
        ReadFile readFile = new ReadFile();
        String pathFolder = "/Users/user/Documents/eclipse-java/data_va";

        try {
            File[] listData = readFile.ReadFileArray(pathFolder);
            for (File file : listData) {
                ReadFileHistory dataFile = new ReadFileHistory();
                String tanggal = file.getName().substring(17, 19) + '/'
                        + file.getName().substring(15, 17) + '/'
                        + file.getName().substring(11, 15);
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
                LocalDateTime now = LocalDateTime.now();

                Stream<String> fileStream = Files.lines(Paths.get(file.getPath()));
                int noOfLines = (int) fileStream.count();
                dataFile.setNamaFile(file.getName());
                dataFile.setTanggalFile(tanggal);
                dataFile.setLoadStart(dtf.format(now));
                dataFile.setUserUploader(authentication.getName());
                dataFile.setTotalRowData(noOfLines);

                ArrayList<BalanceVAHistory> listdata = new ArrayList<BalanceVAHistory>();

                int i=0,j=0,j2=0;
                br = new BufferedReader(new FileReader(file.getAbsolutePath()));
                while ((sCurrentLine = br.readLine()) != null) {
                    String[] elements = sCurrentLine.split("\\s*[|]\\s*");

                    BalanceVAHistory dataVA = new BalanceVAHistory();
                    if (i>=0) {
                        if (sCurrentLine.length() >= maxDataLength) {
                            dataVA.setId(UUID.randomUUID().toString());
                            dataVA.setTanggal(tanggal);
                            dataVA.setNomorVA(elements[0].substring(1));
                            dataVA.setBalanceVA(elements[2]);
                            dataVA.setNamaVA(elements[1].replaceAll("[^a-zA-Z0-9\\s]", "").toUpperCase());

                            if(elements[0].substring(1, 2).equals("8")) {
                                dataVA.setCidVA(elements[0].substring(2, 5));
                            } else if(elements[0].substring(1, 4).equals("988")) {
                                dataVA.setCidVA(elements[0].substring(4, 9));
                            } else if(elements[0].substring(1, 4).equals("989")) {
                                dataVA.setCidVA(elements[0].substring(4, 7));
                            }

                            if(j==0){
                                listdata = new ArrayList<BalanceVAHistory>();
                            }
                            listdata.add(dataVA);
                            j++;
                            if(j==5000){
                                balanceService.loadData(listdata);
                                j=0;
                                j2++;
                                logger.info("Tambah data ke:"+ (j2));
                            }
                        }
                    }
                    i++;
                }
                if(listdata.size()>0){
                    balanceService.loadData(listdata);
                }

                br.close();

                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date date = new Date();
                dataFile.setLoadEnd(formatter.format(date));
                fileRepo.save(dataFile);

                File renameFile = new File(file.getAbsoluteFile()+".Ack");
                file.renameTo(renameFile);
                logger.info("File "+ file.getName()+ " berhasil diinsert");
            }
        } catch(IOException e) {
            logger.info("Error: " + e);
        }

        return "datava/loadData";
    }

    //tampilan data balance_va_history
    public String listVAPaginated(String keyword, String tanggal, ModelMap model, Pageable pageable) {
        List<ReadFileHistory> dataFile = fileService.findAll();
        Page<BalanceVAHistory> page = Page.empty();

        if (keyword == null && tanggal == null) {
            page = Page.empty();
        } else if (tanggal.isEmpty() && !keyword.isEmpty()) {
            page = balanceRepo.findByNomorVAOrCidVA(keyword, keyword, pageable);
        } else if (!tanggal.isEmpty() && keyword.isEmpty()){
            page = balanceService.listByDate(tanggal, pageable);
        } else if (!tanggal.isEmpty() && !keyword.isEmpty()){
            page = balanceRepo.findByTanggalAndNomorVAOrCidVA(tanggal, keyword, keyword, pageable);
        }

        model.addAttribute("data", page);
        model.addAttribute("tgl", dataFile);
        System.out.println(keyword);
        System.out.println(tanggal);

        return "datava/listData";
    }

    @RequestMapping(value = "/web/list-va")
    public String listVA(String keyword, String tanggal, ModelMap model, Pageable pageable) {
        List<BalanceVAHistory> listVA = Collections.EMPTY_LIST;
        model.addAttribute("data", listVA);

        return listVAPaginated(keyword, tanggal, model, pageable);
    }

    //tampilan file yang berhasil diload
    @GetMapping("/web/load-file-history")
    public String listFile(Model model) {
        try {
            List<ReadFileHistory> historyFile = fileService.findAll();
            System.out.println(historyFile);
            model.addAttribute("data", historyFile);
        } catch (Exception e) {
            logger.error("Error data: " + e);
        }

        return "datava/fileHistory";
    }
}
