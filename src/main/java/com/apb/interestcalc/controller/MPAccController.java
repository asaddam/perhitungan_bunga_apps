package com.apb.interestcalc.controller;

import com.apb.interestcalc.config.IAuthenticationFacade;
import com.apb.interestcalc.model.MappingPollingAcc;
import com.apb.interestcalc.repository.MPAccRepository;
import com.apb.interestcalc.service.MPAccService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
@RequestMapping("/web")
public class MPAccController {
    private static Logger logger = LogManager.getLogger(MPAccController.class.getName());

    @Autowired
    private MPAccService mpAccService;

    @Autowired
    private MPAccRepository mpAccRepository;

    @Autowired
    private IAuthenticationFacade auth;

    public String findPaginated(String keyword, Model model, Pageable pageable) {
        Page<MappingPollingAcc> page = mpAccService.listAll(keyword, pageable);

        if (keyword != null) {
            keyword = keyword.toUpperCase();
        }

        model.addAttribute("data", page);
        System.out.println(page.getTotalElements() + " " +page.getSize() );

        return "company/list";
    }

    @RequestMapping("/list")
    public String homePage(String keyword, Model model, Pageable pageable) {
        if (keyword != null) {
            keyword = keyword.toUpperCase();
        }
        List<MappingPollingAcc> listMPAcc = mpAccService.getAllMPAcc();
        model.addAttribute("data", listMPAcc);

        return findPaginated(keyword, model, pageable);
    }

    @GetMapping(value = "/account/detail/{id}")
    public String detailAcc(@PathVariable(value = "id") String id, Model model) {
        MappingPollingAcc mpacc = mpAccService.getById(id);
        model.addAttribute("data", mpacc);
        return "company/detail";
    }

    @GetMapping("/form")
    public String showMPAccInputForm(Model model) {
        MappingPollingAcc mappingPollingAcc = new MappingPollingAcc();
        model.addAttribute("data", mappingPollingAcc);
        return "company/form";
    }

    @PostMapping("/add")
    public String saveMPAcc(@Valid @ModelAttribute("data") MappingPollingAcc mpacc,
                              BindingResult result) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        Authentication authentication = auth.getAuthentication();
        try {
            if (result.hasErrors()) {
                return "company/form";
            }

            mpacc.setTglInput(dtf.format(now));
            mpacc.setUserInput(authentication.getName());

            mpAccService.saveMPAcc(mpacc);

        } catch (Exception e) {
            logger.error("Error data: " + e);
        }

        return "redirect:/web/form?success";
    }

    @GetMapping("/formForUpdate/{id}")
    public String showFormForUpdate(@PathVariable(value = "id") String id, Model model) {
        MappingPollingAcc mpacc = mpAccService.getById(id);
        model.addAttribute("data", mpacc);

        return "company/update";
    }

    @PostMapping("/update")
    public String updateMPAcc(@Valid @ModelAttribute("data") MappingPollingAcc mpacc,
                            BindingResult result) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        Authentication authentication = auth.getAuthentication();
        String id = mpacc.getId();

        try {
            if (result.hasErrors()) {
                return "company/update";
            }
            mpacc.setTglEdit(dtf.format(now));
            mpacc.setUserEdit(authentication.getName());

            mpAccService.saveMPAcc(mpacc);

        } catch (Exception e) {
            logger.error("Error data: " + e);
        }
        String url = "redirect:/web/account/detail/"+ id + "?success=true";

        return url;
    }

        @GetMapping("/deleteList/{id}")
        public String deleteMPAcc(@PathVariable(value = "id") String id) {
            this.mpAccService.deleteMPAccById(id);
            return "redirect:/web/list?deleted=true";
        }
}
