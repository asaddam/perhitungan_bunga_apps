package com.apb.interestcalc.controller;

import com.apb.interestcalc.config.IAuthenticationFacade;
import com.apb.interestcalc.model.BalanceVAHistory;
import com.apb.interestcalc.model.BungaManualInput;
import com.apb.interestcalc.model.BungaVA;
import com.apb.interestcalc.model.ReadFileHistory;
import com.apb.interestcalc.repository.BungaVARepository;
import com.apb.interestcalc.service.BalanceVAHisService;
import com.apb.interestcalc.service.BungaManualService;
import com.apb.interestcalc.service.BungaVAService;
import com.apb.interestcalc.service.ReadFileHisService;
import com.apb.interestcalc.utils.BungaPDFExporter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Controller
public class BungaVAController {
    private static Logger logger = LogManager.getLogger(BalanceVAHisController.class.getName());

    @Autowired
    private BalanceVAHisService balanceService;

    @Autowired
    private ReadFileHisService fileService;

    @Autowired
    private BungaVAService bungaVAService;

    @Autowired
    private BungaManualService bungaManualService;

    @Autowired
    private BungaVARepository bungaVARepo;

    @Autowired
    private IAuthenticationFacade auth;

    // nggak kepake ?
    @RequestMapping(value = "/web/bungava/load-bungava")
    public String loadBunga() {
        List<BalanceVAHistory> dataVA;
        ArrayList<BungaVA> listdata = new ArrayList<>();
        int ukuranPage = 1000;
        int current = 0;

        try {
            List<ReadFileHistory> months = fileService.findMonths();

            for (ReadFileHistory tanggal : months) {
                String bulan = tanggal.getTanggalFile();
                int year = Integer.parseInt(bulan.substring(3, 7));
                int month = Integer.parseInt(bulan.substring(0, 2));
                YearMonth yearMonthObj = YearMonth.of(year, month);
                int daysInMonth = yearMonthObj.lengthOfMonth();
                logger.info("year: "+ year + " month: "+month+ " adays "+ daysInMonth );
                List<BalanceVAHistory> cidVA = balanceService.findCidWithDistinct(bulan);

                for (BalanceVAHistory cid : cidVA) {
                    String companyID = cid.getCidVA();
                    logger.info("cid: "+ cid.getCidVA());
                    Long totalBalanceCID = balanceService.totalBalanceCID(bulan, companyID);

                    int jumlahData = balanceService.count(companyID, bulan);
                    logger.info("jumlah data: "+ jumlahData);

                    int i=0, j=0;
                    while (current < jumlahData) {
                        dataVA = balanceService.findVAWithDistinct(bulan, companyID, current, ukuranPage);
                        while (i < dataVA.size()) {
                            BungaVA bungaVA = new BungaVA();
                            Long totalBalanceVA = balanceService.totalBalanceVA(bulan,
                                    companyID, dataVA.get(i).getNomorVA());
                            Double avgBalanceVA = totalBalanceVA.doubleValue() / daysInMonth;
                            Double persentaseBunga = avgBalanceVA / totalBalanceCID; // bunga flat nggak dipake

                            bungaVA.setBulan(bulan);
                            bungaVA.setCidVA(companyID);
                            bungaVA.setNomorVA(dataVA.get(i).getNomorVA());
                            bungaVA.setTotalBalanceVA(totalBalanceVA);
                            bungaVA.setTotalBalanceCID(totalBalanceCID); // nggk perlu ?
                            bungaVA.setAvgBalanceVA(avgBalanceVA);
                            bungaVA.setPersentaseBunga(persentaseBunga);

//                            logger.info("i: "+i);
                            if(i==0){
                                listdata = new ArrayList<BungaVA>();
                            }
                            listdata.add(bungaVA);
                            i++;
                            if(i==1000){
                                bungaVAService.loadDataBunga(listdata);
                                j++;
                                logger.info("Tambah data ke:"+ (j));
                            }
                        }
                        i=0;
//                        dataVA = new ArrayList<>();
                        current = current + ukuranPage;
                    }
                    if (listdata.size()>0) {
                        bungaVAService.loadDataBunga(listdata);
                    }
                }
            }

        } catch (Exception e) {
            logger.info("Error: " + e);
        }
        return "redirect:/web/bungava/";
    }

    public String listBungaVAPage(String tanggal, String cid, String nomorVA, ModelMap model, Pageable pageable) {
        List<ReadFileHistory> bulan = fileService.findMonths();
        List<BungaVA> companyID = bungaVAService.getDistinctCid();
        Long totalBalanceCID = 0L;
        Long balanceBunga = 0L;
        Page<BungaVA> page = Page.empty();

        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        if (nomorVA == null && tanggal == null) {
            page = Page.empty();
            totalBalanceCID = 0L;
            balanceBunga = 0L;
        } else if (!tanggal.isEmpty() && !cid.isEmpty())  {
            page = bungaVARepo.findByBulanAndCidVA(tanggal, cid, pageable);
            totalBalanceCID = bungaVAService.getTotalBalance(tanggal, cid);
            balanceBunga = Long.valueOf(bungaManualService.getBunga(tanggal, cid));
            logger.info(balanceBunga);
        }
        System.out.println(totalBalanceCID);
        System.out.println(balanceBunga);
        model.addAttribute("bulan", bulan);
        model.addAttribute("cid", companyID);
        model.addAttribute("balanceCID", kursIndonesia.format(totalBalanceCID));
        model.addAttribute("totalBunga", kursIndonesia.format(balanceBunga));
        model.addAttribute("data", page);

        return "datava/listBunga";
    }

    @RequestMapping(value = "/web/bungava/list")
    public String listBungaVA(String tanggal, String cid, String nomorVA,  ModelMap model, Pageable pageable) {
        List<ReadFileHistory> bulan = fileService.findMonths();
        List<BungaVA> companyID = bungaVAService.getDistinctCid();
        Long totalBalanceCID = null;
        Long balanceBunga = null;

        System.out.println(bulan);
        model.addAttribute("bulan", bulan);
        model.addAttribute("cid", companyID);

        List<BungaVA> bungaVA = Collections.EMPTY_LIST;
        model.addAttribute("data", bungaVA);
        model.addAttribute("balanceCID", totalBalanceCID);
        model.addAttribute("balanceBunga", balanceBunga);

        return listBungaVAPage(tanggal, cid, nomorVA, model, pageable);
    }

    @RequestMapping(value = "/web/bungava/form-update-balance")
    public String showFormBalance(Model model) {
        List<ReadFileHistory> bulan = fileService.findMonths();
        List<BungaVA> companyID = bungaVAService.getDistinctCid();

        System.out.println(bulan);
        model.addAttribute("bulan", bulan);
        model.addAttribute("cid", companyID);
        return "datava/loadBunga";
    }

    @RequestMapping("/web/bungava/proses-bunga")
    public String hitungBunga(@RequestParam("balance") Long balance,
                              @RequestParam("bulan") String bulan,
                              @RequestParam("cid") String cid) {
        List<BalanceVAHistory> dataVA;
        ArrayList<BungaVA> listdata = new ArrayList<>();
        int ukuranPage = 1000;
        int current = 0;

    }

    //nggk kepake ?
    @RequestMapping("/web/bungava/update-balance")
    public String updateBalance(@RequestParam("balance") Long balance,
                                @RequestParam("bulan") String bulan,
                                @RequestParam("cid") String cid) {

        try {
            BungaManualInput bungava = new BungaManualInput();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            Authentication authentication = auth.getAuthentication();

            bungava.setBulan(bulan);
            bungava.setCid(cid);
            bungava.setTotalBunga(balance);
            bungava.setUserInput(authentication.getName());
            bungava.setCreatedDate(dtf.format(now));
            bungava.setStatus("ACTIVE");
            bungaManualService.saveBunga(bungava);

            logger.info(balance.toString());

            List<BungaVA> listBesarBunga = new ArrayList<>();
            List<BungaVA> listNomorVA = bungaVAService.getNomorVAandPersenBunga(bulan, cid);
            int i=0, j=0;
            for (BungaVA d : listNomorVA) {
                BungaVA data = new BungaVA();
                String nomorVA = d.getNomorVA();
                Double persenBunga = d.getPersentaseBunga();
                Double besarBungaVA = balance * persenBunga;
                logger.info(nomorVA + " - "+ persenBunga + " - " + besarBungaVA);

                data.setBesarBungaVA(besarBungaVA);
                data.setBulan(bulan);
                data.setNomorVA(nomorVA);
                if(i==0){
                    listBesarBunga = new ArrayList<BungaVA>();
                }
                listBesarBunga.add(data);
                i++;
                if(i==5000){
                    bungaVAService.updateBesarBunga(listBesarBunga);
                    j++;
                    logger.info("Tambah data ke:"+ (j));
                    i=0;
                }
            }
            if (listBesarBunga.size() > 0) {
                bungaVAService.updateBesarBunga(listBesarBunga);
            }
        } catch (Exception e) {
            logger.error("Error data: " + e);
        }

        return "redirect:/web/bungava/list?success";
    }

    @GetMapping("/web/bunga/export-pdf/{tanggal}/{cid}")
    public void exportToPDF(HttpServletResponse response,
                            @PathVariable("tanggal") String tanggal,
                            @PathVariable("cid") String cid,
                            Pageable page) throws DocumentException, IOException {
        response.setContentType("application/pdf");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=users_" + currentDateTime + ".pdf";
        response.setHeader(headerKey, headerValue);

        Page<BungaVA> dataBunga = bungaVARepo.findByBulanAndCidVA(tanggal, cid, page);

        BungaPDFExporter exporter = new BungaPDFExporter(dataBunga.getContent());
        exporter.export(response);

    }

    @RequestMapping("/web/bunga/delete")
    public void deleteBunga(String bulan, String cid) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        Authentication authentication = auth.getAuthentication();

        bungaManualService.deleteBunga(authentication.getName(), dtf.format(now), bulan, cid);
        logger.info("Soft delete success.");
    }

    @RequestMapping("/web/bunga/update")
    public void updateBunga(Long bunga, String status, String bulan, String cid) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        Authentication authentication = auth.getAuthentication();

        bungaManualService.updateBunga(bunga, status, authentication.getName(), dtf.format(now), bulan, cid);
        logger.info("update bunga success.");
    }

    @GetMapping
    public String saveAsPdf() {
        return "";
    }

    @GetMapping("/web/bungava/excel")
    public String saveAsExcel() {
        return "";

    }
}
