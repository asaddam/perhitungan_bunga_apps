package com.apb.interestcalc.controller;

import com.apb.interestcalc.model.Role;
import com.apb.interestcalc.model.User;
import com.apb.interestcalc.repository.InsertRoleRepository;
import com.apb.interestcalc.repository.RoleRepository;
import com.apb.interestcalc.repository.UserRepository;
import com.apb.interestcalc.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Controller
public class MainController {
    private static Logger logger = LogManager.getLogger(MainController.class.getName());

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private InsertRoleRepository insertRoleRepository;

    @GetMapping("/login")
    public String login(Model model) {
        return "login";
    }

    @RequestMapping(value = "/")
    public String loadFirst( Model model){
        return "welcome/welcome";
    }

    @GetMapping("/user/list")
    public String listUser(Model model) {
        try {
            List<User> listUser = userRepository.findAll();
            model.addAttribute("data", listUser);
        } catch (Exception e) {
            logger.error("Error data: " + e);
        }

        return "users/list";
    }

    @GetMapping("/user/detail/{email}")
    public String getUserById(@PathVariable(value = "email") String email, Model model) {
        try {
            User userDetail = userRepository.findByEmail(email);
            ArrayList<String> listRole = new ArrayList<>();
            List<Role> roleUser = roleRepository.listByName(email);
            System.out.println(roleUser);
            for (Role role : roleUser) {
                listRole.add(role.getRole());
            }

            model.addAttribute("data", userDetail);
            model.addAttribute("role", listRole);
        } catch (Exception e) {
            logger.error("Error data: " + e);
        }

        return "users/detail";
    }

    @RequestMapping(value = "/user/add-role")
    public String addRole(@Valid @ModelAttribute("data") User user,
                              @Param("email") String email,
                              @Param("role") String role,
                              BindingResult result) {
        user = userRepository.findByEmail(email);
        Role newRole = new Role();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        newRole.setUsername(user.getEmail());
        newRole.setRole(role);
        newRole.setDate_created(dtf.format(now));

        try {
            insertRoleRepository.insertRole(newRole);
        } catch (Exception e) {
            logger.error("Error data: " + e);
        }
        String url = "redirect:/user/detail/"+ email + "?success=true";

        return url;
    }

    @GetMapping("/user/delete-role/{email}")
    public String deleteRoleUser(@PathVariable(value = "email") String email,
                                 @Param(value = "role") String role) {

        System.out.println(email);
        System.out.println(role);
        this.roleRepository.deleteRole(email, role);
        String url = "redirect:/user/detail/"+ email + "?deleted=true";

        return url;
    }

}
