package com.apb.interestcalc.model;

import javax.persistence.*;

@Entity
@Table(name = "bungava_file_history")
public class ReadFileHistory {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_Sequence")
    @SequenceGenerator(name = "id_Sequence", sequenceName = "FILEID_SEQ")
    private Long id;
    @Column(name = "tanggal")
    private String tanggalFile;
    private String namaFile;
    private int totalRowData;
    private String userUploader;
    private String loadStart;
    private String loadEnd;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserUploader() {
        return userUploader;
    }

    public void setUserUploader(String userUploader) {
        this.userUploader = userUploader;
    }

    public String getLoadStart() {
        return loadStart;
    }

    public void setLoadStart(String loadStart) {
        this.loadStart = loadStart;
    }

    public String getLoadEnd() {
        return loadEnd;
    }

    public void setLoadEnd(String loadEnd) {
        this.loadEnd = loadEnd;
    }

    public String getTanggalFile() {
        return tanggalFile;
    }

    public void setTanggalFile(String tanggalFile) {
        this.tanggalFile = tanggalFile;
    }

    public String getNamaFile() {
        return namaFile;
    }

    public void setNamaFile(String namaFile) {
        this.namaFile = namaFile;
    }

    public int getTotalRowData() {
        return totalRowData;
    }

    public void setTotalRowData(int totalRowData) {
        this.totalRowData = totalRowData;
    }
}
