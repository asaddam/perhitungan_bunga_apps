package com.apb.interestcalc.model;

import javax.persistence.*;

@Entity
@Table(name = "bungava_balance_va_history")
public class BalanceVAHistory {
    @Id
    private String id;
    private String tanggal;
    @Column(name = "nomor_va")
    private String nomorVA;
    @Column(name = "cid_va")
    private String cidVA;
    @Column(name = "balance_va")
    private String balanceVA;
    @Column(name = "nama_va")
    private String namaVA;

    public BalanceVAHistory() {}

    public BalanceVAHistory(String id, String tanggal, String nomorVA, String cidVA, String balanceVA, String namaVA) {
        this.id = id;
        this.tanggal = tanggal;
        this.nomorVA = nomorVA;
        this.cidVA = cidVA;
        this.balanceVA = balanceVA;
        this.namaVA = namaVA;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getNomorVA() {
        return nomorVA;
    }

    public void setNomorVA(String nomorVA) {
        this.nomorVA = nomorVA;
    }

    public String getCidVA() {
        return cidVA;
    }

    public void setCidVA(String cidVA) {
        this.cidVA = cidVA;
    }

    public String getBalanceVA() {
        return balanceVA;
    }

    public void setBalanceVA(String balanceVA) {
        this.balanceVA = balanceVA;
    }

    public String getNamaVA() {
        return namaVA;
    }

    public void setNamaVA(String namaVA) {
        this.namaVA = namaVA;
    }
}
