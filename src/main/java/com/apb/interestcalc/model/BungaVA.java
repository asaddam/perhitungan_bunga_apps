package com.apb.interestcalc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "bungava_persentase_bunga_va")
public class BungaVA {
    @Id
    private String id;
    private String bulan;
    @Column(name = "nomor_va")
    private String nomorVA;
    @Column(name = "cid_va")
    private String cidVA;
    private Long totalBalanceVA;
    private Double avgBalanceVA;
    private Long totalBalanceCID;
    private Double persentaseBunga;
    private Double besarBungaVA;

    public BungaVA() {};

    public BungaVA(String id, String bulan, String nomorVA,
                   String cidVA, Long totalBalanceVA,
                   Double avgBalanceVA, Long totalBalanceCID,
                   Double persentaseBunga,
                   Double besarBungaVA) {
        this.id = id;
        this.bulan = bulan;
        this.nomorVA = nomorVA;
        this.cidVA = cidVA;
        this.totalBalanceVA = totalBalanceVA;
        this.avgBalanceVA = avgBalanceVA;
        this.totalBalanceCID = totalBalanceCID;
        this.persentaseBunga = persentaseBunga;
        this.besarBungaVA = besarBungaVA;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBulan() {
        return bulan;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }

    public String getNomorVA() {
        return nomorVA;
    }

    public void setNomorVA(String nomorVA) {
        this.nomorVA = nomorVA;
    }

    public String getCidVA() {
        return cidVA;
    }

    public void setCidVA(String cidVA) {
        this.cidVA = cidVA;
    }

    public Long getTotalBalanceVA() {
        return totalBalanceVA;
    }

    public void setTotalBalanceVA(Long totalBalanceVA) {
        this.totalBalanceVA = totalBalanceVA;
    }

    public Double getAvgBalanceVA() {
        return avgBalanceVA;
    }

    public void setAvgBalanceVA(Double avgBalanceVA) {
        this.avgBalanceVA = avgBalanceVA;
    }

    public Long getTotalBalanceCID() {
        return totalBalanceCID;
    }

    public void setTotalBalanceCID(Long totalBalanceCID) {
        this.totalBalanceCID = totalBalanceCID;
    }

    public Double getPersentaseBunga() {
        return persentaseBunga;
    }

    public void setPersentaseBunga(Double persentaseBunga) {
        this.persentaseBunga = persentaseBunga;
    }

    public Double getBesarBungaVA() {
        return besarBungaVA;
    }

    public void setBesarBungaVA(Double besarBunga) {
        this.besarBungaVA = besarBunga;
    }
}
