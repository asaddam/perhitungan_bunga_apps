package com.apb.interestcalc.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "BUNGAVA_MAPPING_POLLING_ACC")
public class MappingPollingAcc {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "cid")
    private String cid;

    @Column(name = "nama_company")
    private String namaCompany;

    @Column(name = "rek_polling")
    private String rekPolling;

    @Column(name = "rek_aviliasi")
    private String rekAviliasi;

    @Column(name = "folder_file")
    private String folderFile;

    @Column(name = "tgl_input")
    private String tglInput;

    @Column(name = "tgl_edit")
    private String tglEdit;

    @Column(name = "user_input")
    private String userInput;

    @Column(name = "user_edit")
    private String userEdit;

    @Column(name = "status")
    private String status;

    @Column(name = "delimiter")
    private String delimiter;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getNamaCompany() {
        return namaCompany;
    }

    // store value tolowercase
    public void setNamaCompany(String namaCompany) {
        this.namaCompany = namaCompany.toLowerCase();
    }

    public String getRekPolling() {
        return rekPolling;
    }

    public void setRekPolling(String rekPolling) {
        this.rekPolling = rekPolling;
    }

    public String getRekAviliasi() {
        return rekAviliasi;
    }

    public void setRekAviliasi(String rekAviliasi) {
        this.rekAviliasi = rekAviliasi;
    }

    public String getFolderFile() {
        return folderFile;
    }

    public void setFolderFile(String folderFile) {
        this.folderFile = folderFile;
    }

    public String getTglInput() {
        return tglInput;
    }

    public void setTglInput(String tglInput) {
        this.tglInput = tglInput;
    }

    public String getTglEdit() {
        return tglEdit;
    }

    public void setTglEdit(String tglEdit) {
        this.tglEdit = tglEdit;
    }

    public String getUserInput() {
        return userInput;
    }

    public void setUserInput(String userInput) {
        this.userInput = userInput.toLowerCase();
    }

    public String getUserEdit() {
        return userEdit;
    }

    public void setUserEdit(String userEdit) {
        this.userEdit = userEdit.toLowerCase();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDelimiter() {
        return delimiter;
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    @Override
    public String toString() {
        return "Product [id=" + id
                + ", cid=" + cid
                + ", nama_company=" + namaCompany
                + ", rek_polling=" + rekPolling
                + ", rek_aviliasi=" + rekAviliasi
                + ", folder_file=" + folderFile
                + ", tgl_input=" + tglInput
                + ", tgl_edit=" + tglEdit
                + ", user_input=" + userInput
                + ", user_edit=" + userEdit
                + ", status=" + status
                + ", delimiter=" + delimiter
                + "]";
    }
}
