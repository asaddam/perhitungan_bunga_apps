package com.apb.interestcalc.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReadFileHistoryMapper implements RowMapper<ReadFileHistory> {
    @Override
    public ReadFileHistory mapRow(ResultSet rs, int i) throws SQLException {
        ReadFileHistory dataFile = new ReadFileHistory();
        dataFile.setId(rs.getLong("id"));
        dataFile.setTanggalFile(rs.getString("tanggal"));
        dataFile.setNamaFile(rs.getString("nama_file"));
        dataFile.setTotalRowData(rs.getInt("total_row_data"));
        dataFile.setLoadStart(rs.getString("load_start"));
        dataFile.setLoadEnd(rs.getString("load_end"));
        dataFile.setUserUploader(rs.getString("user_uploader"));

        return dataFile;
    }
}
