package com.apb.interestcalc.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BalanceVAHistoryMapper implements RowMapper<BalanceVAHistory> {

    @Override
    public BalanceVAHistory mapRow(ResultSet rs, int i) throws SQLException {
        BalanceVAHistory dataVA = new BalanceVAHistory();
        dataVA.setId(rs.getString("id"));
        dataVA.setTanggal(rs.getString("tanggal"));
        dataVA.setNomorVA(rs.getString("nomor_va"));
        dataVA.setCidVA(rs.getString("cid_va"));
        dataVA.setBalanceVA(rs.getString("balance_va"));
        dataVA.setNamaVA(rs.getString("nama_va"));

        return dataVA;
    }
}
