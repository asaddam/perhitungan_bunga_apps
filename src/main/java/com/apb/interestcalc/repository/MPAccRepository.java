package com.apb.interestcalc.repository;

import com.apb.interestcalc.model.MappingPollingAcc;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MPAccRepository extends JpaRepository<MappingPollingAcc, Long> {
    Optional<MappingPollingAcc> findById(String id);
    void deleteById(@Param("id") String id);
    Page<MappingPollingAcc> findAll(Specification<MappingPollingAcc> keyword, Pageable page);
}
