package com.apb.interestcalc.repository;

import com.apb.interestcalc.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface RoleRepository extends JpaRepository<Role, Long> {
    @Query(value = "SELECT r from Role r WHERE username LIKE %?1")
    Role findByName(String username);

    @Query(value = "SELECT r from Role r WHERE username LIKE %?1")
    List<Role> listByName(String username);

    @Modifying
    @Query(value = "delete from Role r WHERE r.username = :username and r.role = :role")
    void deleteRole(@Param("username") String username, @Param("role") String role);

}
