package com.apb.interestcalc.repository;

import com.apb.interestcalc.model.BalanceVAHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface BalanceVARepository extends JpaRepository<BalanceVAHistory, Integer> {
    Page<BalanceVAHistory> findAll(Specification<BalanceVAHistory> keyword, Pageable page);
    Page<BalanceVAHistory> findByTanggalAndNomorVAOrCidVA(String tanggal, String keyword1, String keyword2, Pageable page);
    Page<BalanceVAHistory> findByNomorVAOrCidVA(String keyword1, String keyword2, Pageable page);
//    @Query(value = "select distinct b.nomor_va, b.cid_va, b.nama_va " +
//            "from bungava_balance_va_history b " +
//            "WHERE ltrim(TO_CHAR(TO_DATE(b.tanggal, 'DD/MM/YYYY'), 'MM/YYYY'), '1') = :bulan ",
//            nativeQuery = true)
//    Page<BalanceVAHistory> findByDistinct(@Param("bulan") String bulan, Pageable pageable);

}
