package com.apb.interestcalc.repository;

import com.apb.interestcalc.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);

    @Query(value = "SELECT u FROM User u")
    List<User> findAll();

    @Transactional
    @Modifying
    @Query(value = "SELECT u FROM User u WHERE id = ?1")
    Optional<User> findById(Long id);

}
