package com.apb.interestcalc.repository;

import com.apb.interestcalc.model.Role;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class InsertRoleRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void insertRole(Role newRole) {
        entityManager.createNativeQuery("insert into bungava_role_user (username, role, date_created) values (?, ?, ?)")
                .setParameter(1, newRole.getUsername())
                .setParameter(2, newRole.getRole())
                .setParameter(3, newRole.getDate_created())
                .executeUpdate();
    }

    @Transactional
    public void insertWithEntityManager(Role newRole) {
        this.entityManager.persist(newRole);
    }
}
