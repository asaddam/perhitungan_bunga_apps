package com.apb.interestcalc.repository;

import com.apb.interestcalc.model.ReadFileHistory;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileHistoryRepository extends PagingAndSortingRepository<ReadFileHistory, Integer> {
}
