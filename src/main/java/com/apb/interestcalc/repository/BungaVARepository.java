package com.apb.interestcalc.repository;

import com.apb.interestcalc.model.BalanceVAHistory;
import com.apb.interestcalc.model.BungaVA;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BungaVARepository extends JpaRepository<BungaVA, Integer> {
    Page<BungaVA> findAll(Pageable page);
    Page<BungaVA> findByBulanAndCidVAOrNomorVA(String tanggal, String cid, String nomor, Pageable page);
    Page<BungaVA> findByBulanAndCidVA(String tanggal, String cid, Pageable page);

}
