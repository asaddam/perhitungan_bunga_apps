package com.apb.interestcalc.service;

import com.apb.interestcalc.model.BalanceVAHistory;
import com.apb.interestcalc.model.BungaVA;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@Service
public class BungaVAServiceImpl implements BungaVAService {
    public static Logger logger = LogManager.getLogger(BalanceVAHisServiceImpl.class.getName());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int[] loadDataBunga(List<BungaVA> dataBunga) {
        String sql = "insert into bungava_persentase_bunga_va(id, bulan, cid_va, nomor_va, " +
                "avg_balanceva, total_balanceva, total_balancecid, persentase_bunga) values(?,?,?,?,?,?,?,?)";

        return this.jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            public void setValues(PreparedStatement ps, int i) {
                try {
                    ps.setObject(1, UUID.randomUUID().toString());
                    ps.setObject(2, dataBunga.get(i).getBulan());
                    ps.setObject(3, dataBunga.get(i).getCidVA());
                    ps.setObject(4, dataBunga.get(i).getNomorVA());
                    ps.setObject(5, dataBunga.get(i).getAvgBalanceVA());
                    ps.setObject(6, dataBunga.get(i).getTotalBalanceVA());
                    ps.setObject(7, dataBunga.get(i).getTotalBalanceCID());
                    ps.setObject(8, dataBunga.get(i).getPersentaseBunga());

                } catch (SQLException sqle) {
                    logger.info("Error: " + sqle);
                }
            }

            public int getBatchSize() {
                return dataBunga.size();
            }
        });
    }

    @Override
    public Long getTotalBalance(String bulan, String cid) {
        String query = "select DISTINCT total_balancecid " +
                "from bungava_persentase_bunga_va " +
                "WHERE bulan = '"+ bulan +"' AND cid_va = '"+ cid+"'";

        return jdbcTemplate.queryForObject(query, Long.class);
    }

//    @Override
//    public void updateBalanceBunga(Long balance, String bulan, String cid) {
//        String query = "UPDATE bungava_persentase_bunga_va SET balance_bunga = "+balance+" " +
//                        "WHERE bulan = '"+bulan+"' AND cid_va = '"+cid+"'";
//        jdbcTemplate.update(query);
//        return;
//    }


    @Override
    public int[] updateBesarBunga(List<BungaVA> besarBunga) {

        String sql = "UPDATE bungava_persentase_bunga_va SET besar_bungava = ? " +
                "WHERE bulan = ? AND nomor_va = ?";

        return this.jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            public void setValues(PreparedStatement ps, int i) {
                try {
                    ps.setObject(1, besarBunga.get(i).getBesarBungaVA());
                    ps.setObject(2, besarBunga.get(i).getBulan());
                    ps.setObject(3, besarBunga.get(i).getNomorVA());
                } catch (SQLException sqle) {
                    logger.info("Error: " + sqle);
                }
            }

            public int getBatchSize() {
                return besarBunga.size();
            }
        });
    }

    @Override
    public List<BungaVA> getNomorVAandPersenBunga(String bulan, String cidVA) {
        String query = "select nomor_va, persentase_bunga " +
                "from bungava_persentase_bunga_va WHERE " +
                "bulan = '"+bulan+"' AND  cid_va = '"+cidVA+"'";

        List<BungaVA> data = jdbcTemplate.query(query, (resultSet, i) -> {
            BungaVA dataVA = new BungaVA();
            dataVA.setNomorVA(resultSet.getString("nomor_va"));
            dataVA.setPersentaseBunga(resultSet.getDouble("persentase_bunga"));
            return dataVA;
        });
        return data;
    }

    @Override
    public List<BungaVA> getDistinctCid() {
        String query = "select DISTINCT cid_va " +
                "from bungava_persentase_bunga_va";

        List<BungaVA> data = jdbcTemplate.query(query, (resultSet, i) -> {
            BungaVA dataVA = new BungaVA();
            dataVA.setCidVA(resultSet.getString("cid_va"));
            return dataVA;
        });
        return data;
    }

//    @Override
//    public Long getBalanceBunga(String bulan, String cid) {
//        String query = "select DISTINCT balance_bunga " +
//                "from bungava_persentase_bunga_va WHERE " +
//                "bulan = '"+bulan+"' AND cid_va = '"+cid+"'";
//
//        return jdbcTemplate.queryForObject(query, Long.class);
//    }
}
