package com.apb.interestcalc.service;

import com.apb.interestcalc.model.BalanceVAHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BalanceVAHisService {
    int[] loadData(List<BalanceVAHistory> data);
    int count(String cid, String bulan);
    List<BalanceVAHistory> findVAWithDistinct(String bulan, String cid, int current, int ukuranPage);
    List<BalanceVAHistory> findCidWithDistinct(String bulan);
    Long totalBalanceVA(String bulan, String cid, String nomorVA);
    Long avgBalanceVA(String bulan, String cid, String nomorVA);
    Long totalBalanceCID(String bulan, String cid);
    Page<BalanceVAHistory> listByDate(String dateSearch, Pageable page);
    Page<BalanceVAHistory> listAll(String keyword, Pageable pageable);
}
