package com.apb.interestcalc.service;

import com.apb.interestcalc.model.MappingPollingAcc;
import com.apb.interestcalc.repository.MPAccRepository;
import com.apb.interestcalc.utils.SearchSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class MPAccServiceImpl implements MPAccService {
    @Autowired
    private MPAccRepository mpAccRepository;

    @Override
    public List<MappingPollingAcc> getAllMPAcc() {
        return mpAccRepository.findAll();
    }

    @Override
    public MappingPollingAcc getById(String id) {
        Optional<MappingPollingAcc> optional = mpAccRepository.findById(id);
        MappingPollingAcc mpacc = null;

        if (optional.isPresent()) {
            mpacc = optional.get();
        } else {
            throw new RuntimeException("Data dengan id tersebut tidak ditemukan: "+ id);
        }
        return mpacc;
    }

    @Override
    public void saveMPAcc(MappingPollingAcc mpacc) {
        this.mpAccRepository.save(mpacc);
    }

    @Override
    public void deleteMPAccById(String id) {
        this.mpAccRepository.deleteById(id);
    }

    @Override
    public Page<MappingPollingAcc> listAll(String keyword, Pageable pageable) {
        SearchSpecification specification = new SearchSpecification();
        if (keyword != null) {
            Page<MappingPollingAcc> dataPage = mpAccRepository.findAll(Specification.where(specification.containsTextInAccount(keyword)), pageable);
            return dataPage;
        }
        return this.mpAccRepository.findAll(pageable);
    }
}
