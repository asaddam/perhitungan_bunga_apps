package com.apb.interestcalc.service;

import com.apb.interestcalc.model.ReadFileHistory;
import com.apb.interestcalc.repository.FileHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Service
public class ReadFileHisServiceImpl implements ReadFileHisService {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private FileHistoryRepository fileRepo;

    public ReadFileHisServiceImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void save(ReadFileHistory file) {
        String sql = "insert into bungava_file_history(tanggal, nama_file, total_row_data, load_start, load_end, user_uploader) values(?,?,?,?,?,?,?)";
        jdbcTemplate.update(sql, file.getTanggalFile(), file.getNamaFile(), file.getTotalRowData(), file.getLoadStart(), file.getLoadEnd(), file.getUserUploader());
    }

    @Override
    public List<ReadFileHistory> findAll() {
        String sql = "select * from bungava_file_history";
        List<ReadFileHistory> listData = jdbcTemplate.query(sql, new RowMapper<ReadFileHistory>() {
            @Override
            public ReadFileHistory mapRow(ResultSet rs, int rowNum) throws SQLException {
                ReadFileHistory fileData = new ReadFileHistory();

                fileData.setId(rs.getLong("id"));
                fileData.setTanggalFile(rs.getString("tanggal"));
                fileData.setNamaFile(rs.getString("nama_file"));
                fileData.setLoadStart(rs.getString("load_start"));
                fileData.setLoadEnd(rs.getString("load_end"));
                fileData.setUserUploader(rs.getString("user_uploader"));

                return fileData;
            }
        });

        return listData;
    }

    @Override
    public List<ReadFileHistory> findMonths() {
        String query = "select DISTINCT SUBSTR(tanggal,4,7) as month " +
                "from bungava_file_history";
        List<ReadFileHistory> bulan = jdbcTemplate.query(query, (resultSet, i) -> {
            ReadFileHistory file = new ReadFileHistory();
            file.setTanggalFile(resultSet.getString("month"));
            return file;
        });
        return bulan;
    }

}
