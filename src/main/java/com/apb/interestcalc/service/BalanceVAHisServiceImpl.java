package com.apb.interestcalc.service;

import com.apb.interestcalc.model.BalanceVAHistory;
import com.apb.interestcalc.repository.BalanceVARepository;
import com.apb.interestcalc.utils.SearchSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class BalanceVAHisServiceImpl implements BalanceVAHisService {
    public static Logger logger = LogManager.getLogger(BalanceVAHisServiceImpl.class.getName());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private BalanceVARepository vaRepository;

    @Override
    public int count(String cid, String bulan) {
        return jdbcTemplate.queryForObject("SELECT count(distinct nomor_va) FROM BUNGAVA_BALANCE_VA_HISTORY " +
                "WHERE SUBSTR(tanggal,4,7) = '"+ bulan +"' " +
                "AND cid_va = '"+ cid + "' ", Integer.class);
    }

    public BalanceVAHisServiceImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Autowired
    private Environment env;

    @Override
    public List<BalanceVAHistory> findVAWithDistinct(String bulan, String cid, int current, int ukuranPage) {
        String query = "select /*+INDEX(INDEX1)*/ DISTINCT nomor_va " +
                "from bungava_balance_va_history " +
                "WHERE SUBSTR(tanggal,4,7) = '"+ bulan +"' " +
                "AND cid_va = '"+ cid + "' " +
                "OFFSET "+ current +" ROWS FETCH NEXT "+ukuranPage+" ROWS ONLY";
        List<BalanceVAHistory> data = new ArrayList<>();
        try {
            Class.forName(env.getProperty("spring.datasource.driver-class-name"));
            try (Connection con = DriverManager.getConnection(env.getProperty("spring.datasource.url"),
                    env.getProperty("spring.datasource.username"), env.getProperty("spring.datasource.password"));
                 PreparedStatement statement = con.prepareStatement(query);
                 ResultSet rs = statement.executeQuery()) {
                rs.setFetchSize(1000);
                while (rs.next()) {
                    BalanceVAHistory va = new BalanceVAHistory();
                    va.setNomorVA(rs.getString(1));
                    data.add(va);
                }
            } catch (SQLException e) {
                logger.info("Error: " + e);
            }
        } catch (ClassNotFoundException e) {
            logger.info("Error: " + e);
        }

        return data;
    }

    @Override
    public List<BalanceVAHistory> findCidWithDistinct(String bulan) {
        String query = "select DISTINCT cid_va " +
                "from bungava_balance_va_history " +
                "WHERE SUBSTR(tanggal,4,7) = '"+ bulan +"'";

        List<BalanceVAHistory> data = jdbcTemplate.query(query, (resultSet, i) -> {
            BalanceVAHistory dataVA = new BalanceVAHistory();
            dataVA.setCidVA(resultSet.getString("cid_va"));
            return dataVA;
        });
        return data;
    }

    @Override
    public Long totalBalanceVA(String bulan, String cid, String nomorVA) {
        String query = "SELECT /*+INDEX(INDEX1)*/ SUM(balance_va) " +
                "FROM bungava_balance_va_history " +
                "WHERE SUBSTR(tanggal,4,7) = '"+ bulan +"' " +
                "AND cid_va = '"+cid+"' AND nomor_va = '"+nomorVA+"'";
        return jdbcTemplate.queryForObject(query, Long.class);
    }

    @Override
    public Long avgBalanceVA(String bulan, String cid, String nomorVA) {
        String query = "SELECT /*+INDEX(INDEX1)*/ AVG(balance_va) " +
                "FROM bungava_balance_va_history " +
                "WHERE SUBSTR(tanggal,4,7) = '"+ bulan +"' " +
                "AND cid_va = '"+cid+"' AND nomor_va = '"+nomorVA+"'";
        return jdbcTemplate.queryForObject(query, Long.class);
    }

    @Override
    public Long totalBalanceCID(String bulan, String cid) {
        String query = "SELECT SUM(balance_va) FROM bungava_balance_va_history " +
                "WHERE SUBSTR(tanggal,4,7) = '"+ bulan +"' " +
                "AND cid_va = '"+ cid +"'";
        return jdbcTemplate.queryForObject(query, Long.class);
    }

    @Override
    public int[] loadData(List<BalanceVAHistory> data) {
        String sql = "insert into bungava_balance_va_history(id, tanggal, nomor_va, cid_va, balance_va, nama_va) values(?,?,?,?,?,?)";

        return this.jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            public void setValues(PreparedStatement ps, int i) {
                try {
                    ps.setObject(1, data.get(i).getId());
                    ps.setObject(2, data.get(i).getTanggal());
                    ps.setObject(3, data.get(i).getNomorVA());
                    ps.setObject(4, data.get(i).getCidVA());
                    ps.setObject(5, data.get(i).getBalanceVA());
                    ps.setObject(6, data.get(i).getNamaVA());

                } catch (SQLException sqle) {
                    logger.info("Error: " + sqle);
                }
            }

            public int getBatchSize() {
                return data.size();
            }
        });
    }

    @Override
    public Page<BalanceVAHistory> listByDate(String datesearch, Pageable pageable) {
        SearchSpecification specification = new SearchSpecification();
        if (datesearch != null) {
            Page<BalanceVAHistory> dataPage = vaRepository.findAll(Specification.where(specification.containsDateInColumn(datesearch)), pageable);
            return dataPage;
        }

        List<BalanceVAHistory> data = Collections.EMPTY_LIST;
        Page<BalanceVAHistory> dataPage = new PageImpl<BalanceVAHistory>(data, pageable, data.size());
        return dataPage;
    }


    @Override
    public Page<BalanceVAHistory> listAll(String keyword, Pageable pageable) {
        SearchSpecification specification = new SearchSpecification();
        if (keyword != null) {
            Page<BalanceVAHistory> dataPage = vaRepository.findAll(Specification.where(specification.containsTextInColumn(keyword)), pageable);
            return dataPage;
        }

        List<BalanceVAHistory> data = Collections.EMPTY_LIST;
        Page<BalanceVAHistory> dataPage = new PageImpl<BalanceVAHistory>(data, pageable, data.size());
        return dataPage;
    }

//    private BalanceVAHistory mapUserResult(final ResultSet rs) throws SQLException {
//        BalanceVAHistory listVA = new BalanceVAHistory();
//        listVA.setId(rs.getString("id"));
//        listVA.setTanggal(rs.getString("tanggal"));
//        listVA.setNomorVA(rs.getString("nomor_va"));
//        listVA.setCidVA(rs.getString("cid_va"));
//        listVA.setBalanceVA(rs.getString("balance_va"));
//        listVA.setNamaVA(rs.getString("nama_va"));
//
//        return listVA;
//    }
}
