package com.apb.interestcalc.service;

import com.apb.interestcalc.model.UserRegistrationDto;
import com.apb.interestcalc.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {
    User findByEmail(String email);
    List<User> getAllUser();

    User save(UserRegistrationDto registration);
}
