package com.apb.interestcalc.service;

import com.apb.interestcalc.model.ReadFileHistory;

import java.util.List;

public interface ReadFileHisService {
    void save(ReadFileHistory dataFile);
    List<ReadFileHistory> findAll();
    List<ReadFileHistory> findMonths();
}
