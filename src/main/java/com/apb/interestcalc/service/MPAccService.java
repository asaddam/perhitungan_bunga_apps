package com.apb.interestcalc.service;

import com.apb.interestcalc.model.MappingPollingAcc;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MPAccService {
    MappingPollingAcc getById(String id);
    List<MappingPollingAcc> getAllMPAcc();
    void saveMPAcc(MappingPollingAcc mpacc);
    void deleteMPAccById(String id);
    Page<MappingPollingAcc> listAll(String keyword, Pageable pageable);
}
