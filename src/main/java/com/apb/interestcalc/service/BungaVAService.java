package com.apb.interestcalc.service;

import com.apb.interestcalc.model.BungaVA;

import java.util.List;

public interface BungaVAService {
    int[] loadDataBunga(List<BungaVA> dataBunga);
    int[] updateBesarBunga(List<BungaVA> besarBunga);
    List<BungaVA> getDistinctCid();
    List<BungaVA> getNomorVAandPersenBunga(String bulan, String cidVA);
    Long getTotalBalance(String bulan, String cid);
//    Long getBalanceBunga(String bulan, String cid);
//    void updateBalanceBunga(Long balance, String bulan, String cid);
//    void updateBesarBunga(Double bunga, String bulan, String cid, String nomorVA);
}
