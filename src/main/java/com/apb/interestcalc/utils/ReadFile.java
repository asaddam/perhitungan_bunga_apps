package com.apb.interestcalc.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class ReadFile {
    private static Logger logger = LogManager.getLogger(ReadFile.class.getName());

    public File[] ReadFileArray(String path) {
        FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".txt");
            }
        };

        File folder = new File(path);
        File[] listOfFiles = folder.listFiles(filter);

        return listOfFiles;
    }
}
