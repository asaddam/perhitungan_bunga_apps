package com.apb.interestcalc.utils;

import com.apb.interestcalc.model.BalanceVAHistory;
import com.apb.interestcalc.model.MappingPollingAcc;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Arrays;
import java.util.List;

public class SearchSpecification {

    public static Specification<BalanceVAHistory> textInAttributes(String text, List<String> attr) {
        if(!text.contains("%")) {
            text = "%" + text + "%";
        }
        final String finalText = text;

        return ((root, query, builder) -> builder.or(root.getModel().getDeclaredSingularAttributes().stream()
                .filter(a -> attr.contains(a.getName()))
                .map(a -> builder.like(root.get(a.getName()), finalText))
                .toArray(Predicate[]::new)
        ));
    }

    public static Specification<BalanceVAHistory> containsTextInColumn(String text) {
        return textInAttributes(text, Arrays.asList("namaVA", "nomorVA", "cidVA"));
    }
    public static Specification<BalanceVAHistory> containsDateInColumn(String text) {
        return textInAttributes(text, Arrays.asList("tanggal"));
    }

    public static Specification<MappingPollingAcc> textInAttr(String text, List<String> attr) {
        if(!text.contains("%")) {
            text = "%" + text + "%";
        }
        final String finalText = text;

        return ((root, query, builder) -> builder.or(root.getModel().getDeclaredSingularAttributes().stream()
                .filter(a -> attr.contains(a.getName()))
                .map(a -> builder.like(root.get(a.getName()), finalText))
                .toArray(Predicate[]::new)
        ));
    }


    public static Specification<MappingPollingAcc> containsTextInAccount(String text) {
        return textInAttr(text, Arrays.asList("cid", "namaCompany", "status"));
    }


}
